import sys

try:
    a = open("geoList.txt")
except FileNotFoundError:
    print("Файл не найден")
    input("Нажмите на enter...")
    sys.exit()
    
cities = a.read().lower().split()
print("Список загружен. Городов: ", len(cities))
a.close()
print("Введите 'выход' для выхода. Let the battle begin!")
last = 1

def user_input():
    user_in = input().lower()
    if user_in == "выход":
        sys.exit()
    else:
        while not(user_in in cities and isLegit(last, user_in[0])):
            print("Нет города с таким названием или уже сыграна.")
            user_in = input().lower()
    cities.remove(user_in)
    return user_in

def isLegit(city_name, lastC):
    if last == 1 :
        return True
    if city_name[len(city_name)-1] == "ь" or city_name[len(city_name)-1] == "ъ" or city_name[len(city_name)-1] == "ё" or city_name[len(city_name)-1] == "ы":    
        cond = city_name[len(city_name)-2] == lastC
    else:
        cond = city_name[len(city_name)-1] == lastC
    return cond


while True:
    user_in = user_input()
    for i in cities:
        cond = isLegit(user_in, i[0])
        if cond:
            print(i)
            last = i[len(i)-1]
            cities.remove(i)
            break

        
